using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lekcje : MonoBehaviour
{

    [SerializeField] Text trescZadaniaTxt;
    [SerializeField] Text txtPunkty;
    [SerializeField] Button[] przyciskiOdpowiedzi;
    [SerializeField] Zadania[] zadaniaLekcjaNr0;
    [SerializeField] Zadania[] zadaniaLekcjaNr1;
    [SerializeField] Text txtNrZadania;
    [SerializeField] Text nrLekcji;
    [SerializeField] GameObject panelUkonczeniaLekcji;

    Zadania[] aktualnaLekcja;
    int numerZadania = 0;
    int numerAktualnejLekcji = 0;
    int iloscOdpowiedziWZadaniu;
    string odpowiedzUzytkownika;
    Punkty punktySkrypt;
    GameManager gm;
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        print(numerZadania);
        print(gm.ZworIloscUkonczonychZadan());
       
        numerAktualnejLekcji = gm.ZwrocNrLekcji();
        UstawLekcje(numerAktualnejLekcji - 1);
        punktySkrypt = FindObjectOfType<Punkty>();
        StworzNoweZadanie();
        WyswietlNumerZadania();
        WyswietlPunkty();
    }


    void Update()
    {
        
    }
    void UstawLekcje(int numerLekcji)
    {
        numerAktualnejLekcji = numerLekcji;
        nrLekcji.text = "Lekcja: " + numerLekcji;
        switch (numerLekcji)
        {
            case 0:
                aktualnaLekcja = zadaniaLekcjaNr0;
                break;
            case 1:
                aktualnaLekcja = zadaniaLekcjaNr1;
                break;

        }
            
        aktualnaLekcja = zadaniaLekcjaNr0;

    }
    //sprawdzenie ilosc buttonow pustych
    void SprawdzIloscOdpowiedziWZadaniu()
    {
        
        iloscOdpowiedziWZadaniu = 0;
        //wykonaj fukcje,  jesli i bedzie mniejsze niz 3, zwieksz co petle i++
        for (int i = 0; i < 3; i++)
        {
            //jesli numer odpowiedzi  nie jest rowny pustemu stringowi
            if (aktualnaLekcja[numerZadania].PobierzZlaOdpowiedz(i + 1) != "" )
            {
                //to dodaj ilosci odpowiedzi
                iloscOdpowiedziWZadaniu++;
            }
        }
        iloscOdpowiedziWZadaniu++;
       // print(iloscOdpowiedziWZadaniu);
    }
    void UkryjPrzyciskiOdpowiedzi()
    {
        //ilosc przycikow odpowiedzi
        for(int i = 0; i < przyciskiOdpowiedzi.Length; i++)
        {
            //ustaw ilosc na false
            przyciskiOdpowiedzi[i].gameObject.SetActive(false);
        }
    }
    void PokazPrzyciskiOdpowiedzi()
    {
        //sprawdz ilosci odpowiedzi z poprzedniej funckji for
        for (int i = 0; i < iloscOdpowiedziWZadaniu; i++)
        {
            //ustaw te buttony na true
            przyciskiOdpowiedzi[i].gameObject.SetActive(true);
        }
    }

    void StworzNoweZadanie()
    {
        odpowiedzUzytkownika = "___";
        UkryjPrzyciskiOdpowiedzi();
        SprawdzIloscOdpowiedziWZadaniu();
        PokazPrzyciskiOdpowiedzi();
        PrzpiszNumerDoPrzyciskow();
        PrzypiszElementyDoZadania();
        WyswietlNumerZadania();
    }
    void PrzypiszElementyDoZadania()
    {
        //replece zamienia cos na na cos
        trescZadaniaTxt.text = aktualnaLekcja[numerZadania].PobierzTrescZadania().Replace("__", odpowiedzUzytkownika);
    }
    void PrzpiszNumerDoPrzyciskow()
    {
        //stworzenie listy o typie string
        //do listy mozna dodawac i usuwac elementy
        List<string> listaOdpowiedzi = new List<string>();

        for(int i = 0; i < iloscOdpowiedziWZadaniu; i++)
        {
            //na samym pocz�tku petli bo 0
            if (i == 0)
            {
                //dodaj to listy poprawna odpowiedz
                listaOdpowiedzi.Add(aktualnaLekcja[numerZadania].PobierzPoprawnaOdpowiedz());
            }
            else
            {
                listaOdpowiedzi.Add(aktualnaLekcja[numerZadania].PobierzZlaOdpowiedz(i));
            }
        }

        int j = 0;
        //jesli ilosc elementow w liscie odpowiedzi jest wieksza niz 0 to wykonuj funckje
        for(int i = listaOdpowiedzi.Count; i > 0; i--)
        {
            //losowa liczba od 0 do ilosci elementw w liscie
            int rand = Random.Range(0, listaOdpowiedzi.Count);
            //przypisz element z listy odpowiedzi do przycisku 
            przyciskiOdpowiedzi[j].GetComponentInChildren<Text>().text = listaOdpowiedzi[rand];
            //usun ta odpowiedz z listy ostatniej czyli rand
            listaOdpowiedzi.RemoveAt(rand);
             // zwieksz j aby po przejsciu petli zwiekszyl sie kolejny przycisk 
            j++;
        }

    }

    public void PobierzOdpowiedzUzytkownika(string odp)
    {
        odpowiedzUzytkownika = odp;
        PrzypiszElementyDoZadania();
    }
    public void SprawdzOdpowiedz()
    {
        if (odpowiedzUzytkownika == aktualnaLekcja[numerZadania].PobierzPoprawnaOdpowiedz())
        {
            print("dobrze");
            if (numerAktualnejLekcji == gm.ZworIloscUkonczonychZadan())
            {
                punktySkrypt.DodajPunkty(1);
            }
            WyswietlPunkty();
            PrzelaczNoweZadanie();
        }
        else
        {
            print("zle");
        }
    }
    void WyswietlPunkty()
    {

        txtPunkty.text = "Liczba punkt�w: " + punktySkrypt.ZwrocIloscPkt();
    }
    void PrzelaczNoweZadanie()
    {
        if (numerZadania < aktualnaLekcja.Length - 1)
        {
            numerZadania++;
            StworzNoweZadanie();
            
        }
        else
        {
            WlaczZakonczenieLekcji(true);
   
        }

    }
    void WlaczZakonczenieLekcji(bool aktywne)
    {
        panelUkonczeniaLekcji.SetActive(aktywne);
        if (numerAktualnejLekcji == gm.ZworIloscUkonczonychZadan())
        {
            gm.DodajIloscUkonczonychZadan();
        }

       
    }
    public void ZamknijPanelLekcji()
    {
        panelUkonczeniaLekcji.SetActive(false);
        WlaczMenu();
    }
    void WyswietlNumerZadania()
    {
        txtNrZadania.text = "Numer zadania: " + numerZadania.ToString();
    }
    public void WlaczMenu()
    {
        gm.ZmianaSceny(0);
    }
}
