using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] GameObject btnPrefab;
    [SerializeField] Transform panelRodzic;
    int iloscWszystkichLekcji = 3;
    // Start is called before the first frame update
    void Start()
    {
        StworzPrzyciskiWyboruLekcji();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void StworzPrzyciskiWyboruLekcji()
    {
        for (int i = 0; i < iloscWszystkichLekcji; i++)
        {
            GameObject btn = Instantiate(btnPrefab, panelRodzic);
            btn.GetComponentInChildren<Text>().text = (i + 1).ToString();
        }
    }
    public void ZresetujPlayerPrefabs()
    {
        FindObjectOfType<GameManager>().ZresetujIloscUkonczonychZadan();
        FindObjectOfType<Punkty>().ZresetujIloscPkt();

    }
 
}
