using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    int numerLekcji;
    int iloscUkonczonychZadan = 0;


    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void Awake()
    {
        //stworz nowy klucz/wczytaj
        if (!PlayerPrefs.HasKey("SaveiloscUkonczonychZadan"))
        {
            PlayerPrefs.SetInt("SaveiloscUkonczonychZadan", 0);
            iloscUkonczonychZadan = 0;
        }
        else
        {
            iloscUkonczonychZadan = PlayerPrefs.GetInt("iloscUkonczonychZadan");
        }



        //sprawdz ilosc game managerow
        if (FindObjectsOfType<GameManager>().Length > 1)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    public int ZworIloscUkonczonychZadan()
    {
        //zwroc ilosc
        return PlayerPrefs.GetInt("iloscUkonczonychZadan");
     
    }
    public void DodajIloscUkonczonychZadan()
    {
        //wczytaj obecna ilosc z zmiennej
        iloscUkonczonychZadan = PlayerPrefs.GetInt("iloscUkonczonychZadan");
        //dodaj zmienna 
        iloscUkonczonychZadan++;
        //przypisz ilosc z zmiennej do klucza 
        PlayerPrefs.SetInt("iloscUkonczonychZadan", iloscUkonczonychZadan);
    }
    public void ZresetujIloscUkonczonychZadan()
    {
        PlayerPrefs.SetInt("iloscUkonczonychZadan", 0);
    }

    public int ZwrocNrLekcji()
    {
        return numerLekcji;
    }
    public void PobierzNrLekcjiZPrzycisku(int nrLekcji)
    {
        numerLekcji = nrLekcji;
        ZmianaSceny(1);

    }
    public void ZmianaSceny(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }
}
