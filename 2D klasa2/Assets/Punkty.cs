using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Punkty : MonoBehaviour
{
    private void Awake()
    {
        //fukcja zapisujaca na stale ilosc pkt
        //jesli nie ma stworzonego klucza playerprefs o tej nazwie
        if (!PlayerPrefs.HasKey("PunktyKey"))
        {
            //to stworz int o tej nazwie i wartoci 0
            PlayerPrefs.SetInt("PunktyKey", 0);
            iloscPunkow = 0;
        }
        else
        {
            //w przeciwnik wypadku, wczytaj ilosc pkt z klucza
            iloscPunkow = PlayerPrefs.GetInt("PunktyKey");
        }
      

    }

    int iloscPunkow;
    // Start is called before the first frame update

    public void DodajPunkty(int punkt)
    {
        //wczytaj
        iloscPunkow = PlayerPrefs.GetInt("PunktyKey");
        //dodaj pkt
        iloscPunkow += punkt;
        //dodaj pkt do klucza 
        PlayerPrefs.SetInt("PunktyKey", iloscPunkow);

 

    }
    public int ZwrocIloscPkt()
    {
        //zwroc ilosc pkt z klucza 
        return PlayerPrefs.GetInt("PunktyKey");
    }
    public void ZresetujIloscPkt()
    {
        PlayerPrefs.SetInt("PunktyKey", 0);
    }
}
