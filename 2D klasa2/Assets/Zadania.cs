using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Zadania", menuName = "Stworz nowe zadanie")]
public class Zadania : ScriptableObject
{
    [SerializeField] int typZadania;
    //zwiekszenie text area w inspektorze unity
    [TextArea(10, 100)]
    [SerializeField] string trescZadania;
    [SerializeField] string poprawnaOdpowiedz;
    [SerializeField] string zlaOdpowiedz1;
    [SerializeField] string zlaOdpowiedz2;
    [SerializeField] string zlaOdpowiedz3;

    public string PobierzTrescZadania()
    {
        return trescZadania;
    }
    public string PobierzPoprawnaOdpowiedz()
    {
        return poprawnaOdpowiedz;
    }
    public string PobierzZlaOdpowiedz(int numerOdpowiedzi)
    {
        if (numerOdpowiedzi == 1)
        {
            return zlaOdpowiedz1;
        }
        else if (numerOdpowiedzi == 2)
        {
            return zlaOdpowiedz2;
        }
        else if (numerOdpowiedzi == 3)
        {
            return zlaOdpowiedz3;
        }
        else
        {
            return "B��d!";
        }

    }
}
