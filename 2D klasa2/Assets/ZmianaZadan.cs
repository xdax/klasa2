using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZmianaZadan : MonoBehaviour
{
    int numerLekcji;
    int iloscUkonczonychLekcji;
    [SerializeField] Image img;
    Button btnZmianaLekcji;
    // Start is called before the first frame update
    void Start()
    {
        btnZmianaLekcji = GetComponent<Button>();
        btnZmianaLekcji.onClick.AddListener(() =>  FindObjectOfType<GameManager>().PobierzNrLekcjiZPrzycisku(System.Convert.ToInt32(GetComponentInChildren<Text>().text)));
        numerLekcji = System.Convert.ToInt32(GetComponentInChildren<Text>().text);
        iloscUkonczonychLekcji = FindObjectOfType<GameManager>().ZwrocNrLekcji();

        if (numerLekcji -1 > iloscUkonczonychLekcji)
        {
            GetComponent<Button>().interactable = false;
            img.gameObject.SetActive(true);
        }
        else
        {
            GetComponent<Button>().interactable = true;
            img.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
