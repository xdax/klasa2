using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Przyciski : MonoBehaviour
{

    Text buttonTxt;
    void Start()
    {
        buttonTxt = GetComponentInChildren<Text>();
        //onClick.AddListener(() czekanie na klikniecie uzytkownika
        //znalezieniu skryptu lekcje, pobranie fukcji i jako argument przypisanie wartosc z buttona 
        GetComponent<Button>().onClick.AddListener(() => FindObjectOfType<Lekcje>().PobierzOdpowiedzUzytkownika(buttonTxt.text));

    }
}
